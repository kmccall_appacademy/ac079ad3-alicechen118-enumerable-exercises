require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  if arr.empty?
    0
  else
    arr.reduce(:+)
  end
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? do |string|
    string.include?(substring)
  end
end

def sub_tring?(string, substring)
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  string_letters = string.chars
  non_uniques = []
  string_letters.map do |char|
    if string_letters.count(char) > 1 && char != " " && !non_uniques.include?(char)
      non_uniques << char
    end
  end
  non_uniques
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  string.split.sort_by { |word| word.length }[-2..-1]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  letters = %w(a b c d e f g h i j k l m n o p q r s t u v w x y z)
  string.each_char { |char| letters.delete(char) }
  letters
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).reduce([]) do |no_repeats, year|
    if not_repeat_year?(year)
      no_repeats << year
    else
      no_repeats
    end
  end
end

def not_repeat_year?(year)
  digits = year.to_s.chars
  i = 0
  while i < (digits.length - 1)
    j = i + 1
    while j < digits.length
      if digits[i] == digits[j]
        return false
      end
      j += 1
    end
    i += 1
  end
  true
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  songs.reduce([]) do |wonders, song|
    if no_repeats?(song, songs) && !wonders.include?(song)
      wonders << song
    else
      wonders
    end
  end
end

def no_repeats?(song_name, songs)
  songs.map.with_index do |song, idx|
    if song == song_name
      if songs[idx + 1] == song_name
        return false
      end
    end
  end
  true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  string.delete!("!?.,")
  c_words = string.downcase.split.select do |word|
    word.include?("c")
  end
  return " " if c_words.empty?
  c_words.sort_by { |word| c_distance(word) }[0]
end

def c_distance(word)
  word.reverse.index("c")
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  ranges = []
  start_idx = nil
  arr.each_with_index do |el, idx|
    if el == arr[idx + 1]
      if start_idx == nil
        start_idx = idx
      end
    elsif start_idx != nil
      ranges << [start_idx, idx]
      start_idx = nil
    end
  end
  ranges
end
